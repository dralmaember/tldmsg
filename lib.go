/*
This is a simple package to implement the TLDMP protocol.

More info on the protocol: https://files.almaember.com/tld-messages.txt
*/
package tldmsg

import (
	"encoding/binary"
	"io"
)

// A simple message structure.
//
// The content length is implied from the length of the slice.
type Message struct {
	// The type of the message
	MType uint8
	// The contents
	Content []byte
}

const (
	// The input didn't contain a full message
	InputTooShort Error = iota
)

// Error type specifically for tldmsg
type Error uint8

// Get a textual representation of the error
func (self Error) Error() string {
	switch self {
	case InputTooShort:
		return "Input too short"
	}

	return "Invalid error"
}

// Write a message to a stream
func (msg Message) WriteToStream(wr io.Writer) error {
	leLen := make([]byte, 8)
	binary.LittleEndian.PutUint64(leLen, uint64(len(msg.Content)))
	_, err := wr.Write([]byte{msg.MType})
	if err != nil {
		return err
	}
	_, err = wr.Write(leLen)
	if err != nil {
		return err
	}
	_, err = wr.Write(msg.Content)
	if err != nil {
		return err
	}

	return nil
}

// Read a message from a stream
func ReadFromStream(rd io.Reader) (Message, error) {
	result := Message{}
	// read the first nine bytes only
	// (mtype + length)
	buf := make([]byte, 9)
	read, err := io.ReadAtLeast(rd, buf, 9)
	if err != nil {
		return result, err
	}
	if read < 9 {
		// we didn't read a full header. Well, shit.
		return result, InputTooShort
	}

	result.MType = buf[0]
	contentLen := binary.LittleEndian.Uint64(buf[1:])
	result.Content = make([]byte, contentLen)

	read, err = io.ReadAtLeast(rd, result.Content, int(contentLen))
	if err != nil {
		return result, err
	}
	if read < int(contentLen) {
		return result, InputTooShort
	}
	return result, nil
}
